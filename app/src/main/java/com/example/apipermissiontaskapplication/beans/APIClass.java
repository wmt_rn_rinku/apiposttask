package com.example.apipermissiontaskapplication.beans;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIClass {

@SerializedName("countryCode")
@Expose
private String countryCode;
@SerializedName("mobileNo")
@Expose
private String mobileNo;
@SerializedName("password")
@Expose
private String password;
@SerializedName("fcmToken")
@Expose
private String fcmToken;
@SerializedName("deviceType")
@Expose
private String deviceType;
@SerializedName("deviceName")
@Expose
private String deviceName;

public String getCountryCode() {
return countryCode;
}

public void setCountryCode(String countryCode) {
this.countryCode = countryCode;
}

public String getMobileNo() {
return mobileNo;
}

public void setMobileNo(String mobileNo) {
this.mobileNo = mobileNo;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public String getFcmToken() {
return fcmToken;
}

public void setFcmToken(String fcmToken) {
this.fcmToken = fcmToken;
}

public String getDeviceType() {
return deviceType;
}

public void setDeviceType(String deviceType) {
this.deviceType = deviceType;
}

public String getDeviceName() {
return deviceName;
}

public void setDeviceName(String deviceName) {
this.deviceName = deviceName;
}

}