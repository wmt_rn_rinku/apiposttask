package com.example.apipermissiontaskapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragment= LoginFragment()
        supportFragmentManager.beginTransaction().replace(R.id.Framlayout, fragment).commit()

        intiView()

    }

    private fun intiView() {
        txtSignup.setOnClickListener {
            val fragment= EditProfFragment()
            supportFragmentManager.beginTransaction().replace(R.id.Framlayout, fragment).commit()

            txtLogin.visibility=View.GONE
            txtSignup.visibility=View.GONE
            imgIcon.visibility=View.GONE
        }
    }
}