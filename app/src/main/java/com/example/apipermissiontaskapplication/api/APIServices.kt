package com.example.apipermissiontaskapplication.api


import com.example.apipermissiontaskapplication.beans.APIClass
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.POST




interface APIServices {

   @POST("login")

    fun getData(
        @Field("mobileNo") mobilenum: String?,
        @Field("password") password: String?,

    ): Call<APIClass>



}